(ns advent-of-code.aoc2015.day06-test
  (:refer-clojure :exclude [* - + / == not= min max])
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015.day06 :refer :all])
  (:require [advent-of-code.aoc2015 :refer :all])
  (:require clojure.core.matrix.impl.ndarray
            [clojure.core.matrix :refer :all]
            [clojure.core.matrix.operators :refer :all])
  (:import (advent_of_code.aoc2015.day06 GridBrightness)))

(deftest helper-test
  (is (= 6 (int (esum (generate-matrix (vector 10 10) 1 3 2 5 1 0)))))
  (is (= 1000000 (int (esum (generate-matrix (vector 1000 1000) 0 0 999 999 1 0)))))
  )

(deftest grid-base-test
  (let [base-grid (initialise-grid 10 5)]
    (is (= [10 5] (.get-size base-grid)))
    (is (= [10 5] (.get-size (.turn-on base-grid 0 0 10 5))))
    (is (reduce (fn [x y] (and x y)) (eseq (.grid (.turn-on base-grid 0 0 10 5)))))
    (is (not (reduce (fn [x y] (or x y)) (eseq (.grid (.turn-off base-grid 0 0 10 5))))))
    (is (reduce (fn [x y] (and x y)) (eseq (.grid (.toggle base-grid 0 0 10 5)))))
    (is (= 0 (.evaluate base-grid)))
    (is (= 50 (.evaluate (.turn-on base-grid 0 0 10 5))))
    (is (= 4 (.evaluate (.turn-on base-grid 3 3 4 4))))
    (is (= 46 (.evaluate (.turn-off (turn-on base-grid 0 0 10 5) 3 3 4 4))))
    (is (= 4 (.evaluate (.toggle base-grid 3 3 4 4))))
    (is (= 11 (.evaluate (.toggle (.turn-on base-grid 1 1 3 3) 3 3 4 4))))
    )
  )

;turn on 0,0 through 999,999 would turn on (or leave on) every light.
;toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
;turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.
(deftest follow-santas-instruction-test
  (let [base-grid-off (initialise-grid 1000 1000)
        base-grid-on (.turn-on base-grid-off 0 0 999 999)]
    (is (= 1000000 (.evaluate (follow-santas-instruction base-grid-off "turn on 0,0 through 999,999"))))
    (is (= 1000000 (.evaluate (follow-santas-instruction base-grid-on "turn on 0,0 through 999,999"))))
    (is (= 1000 (.evaluate (follow-santas-instruction base-grid-off "toggle 0,0 through 999,0"))))
    (is (= (- 1000000 1000) (.evaluate (follow-santas-instruction base-grid-on "toggle 0,0 through 999,0"))))
    (is (= 0 (.evaluate (follow-santas-instruction base-grid-off "turn off 499,499 through 500,500"))))
    (is (= (- 1000000 4) (.evaluate (follow-santas-instruction base-grid-on "turn off 499,499 through 500,500"))))
    )
  )

(deftest puzzle1
  (let [start-grid (initialise-grid 1000 1000)]
    (is (= 377891 (.evaluate
               (reduce follow-santas-instruction start-grid
                       (read-lines "resources/aoc2015/day06_puzzle1.txt")
                       )))
        )
    )
  )

; all the above tests adapted plus new tests
;turn on 0,0 through 0,0 would increase the total brightness by 1.
;toggle 0,0 through 999,999 would increase the total brightness by 2000000
(deftest brightness-test
  (let [^GridBrightness base-grid-off (initialise-brightness-grid 1000 1000)
        ^GridBrightness base-grid-on (.turn-on base-grid-off 0 0 999 999)]
    (is (= 1000000 (.evaluate (follow-santas-instruction base-grid-off "turn on 0,0 through 999,999"))))
    (is (= 2000000 (.evaluate (follow-santas-instruction base-grid-on "turn on 0,0 through 999,999"))))
    (is (= 2000 (.evaluate (follow-santas-instruction base-grid-off "toggle 0,0 through 999,0"))))
    (is (= (+ 1000000 2000) (.evaluate (follow-santas-instruction base-grid-on "toggle 0,0 through 999,0"))))
    (is (= 0 (.evaluate (follow-santas-instruction base-grid-off "turn off 499,499 through 500,500"))))
    (is (= (- 1000000 4) (.evaluate (follow-santas-instruction base-grid-on "turn off 499,499 through 500,500"))))
    (is (= 1 (.evaluate (follow-santas-instruction base-grid-off "turn on 0,0 through 0,0"))))
    (is (= 2000000 (.evaluate (follow-santas-instruction base-grid-off "toggle 0,0 through 999,999"))))
    )
  )

(deftest puzzle2
  (let [^GridBrightness start-grid (initialise-brightness-grid 1000 1000)]
    (is (= 14110788 (.evaluate
                    (reduce follow-santas-instruction start-grid
                            (read-lines "resources/aoc2015/day06_puzzle1.txt")
                            )))
        )
    )
  )
