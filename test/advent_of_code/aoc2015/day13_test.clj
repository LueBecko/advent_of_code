(ns advent-of-code.aoc2015.day13-test
  (:require [clojure.test :refer :all])
  (:refer-clojure :exclude [* - + / == not= min max])
  (:require clojure.core.matrix.impl.ndarray
            [clojure.core.matrix :refer :all]
            [clojure.core.matrix.operators :refer :all])
  (:require [advent-of-code.aoc2015.day13 :refer :all])
  (:require [advent-of-code.aoc2015 :refer :all]))

(deftest optimal_seating_arrangement_test
  (let [example_data ["Alice would gain 54 happiness units by sitting next to Bob."
                      "Alice would lose 79 happiness units by sitting next to Carol."
                      "Alice would lose 2 happiness units by sitting next to David."
                      "Bob would gain 83 happiness units by sitting next to Alice."
                      "Bob would lose 7 happiness units by sitting next to Carol."
                      "Bob would lose 63 happiness units by sitting next to David."
                      "Carol would lose 62 happiness units by sitting next to Alice."
                      "Carol would gain 60 happiness units by sitting next to Bob."
                      "Carol would gain 55 happiness units by sitting next to David."
                      "David would gain 46 happiness units by sitting next to Alice."
                      "David would lose 7 happiness units by sitting next to Bob."
                      "David would gain 41 happiness units by sitting next to Carol."]
        ]
      (is (= 330.0 (optimal_seating_arrangement (parse_happiness_matrix example_data))))
    )
  )

(deftest puzzle1
  (let [puzzle_input (read-lines "resources/aoc2015/day13_puzzle.txt")]
    (is (= 664.0 (optimal_seating_arrangement (parse_happiness_matrix puzzle_input))))
    )
  )

(deftest puzzle2
  (let [puzzle_input (read-lines "resources/aoc2015/day13_puzzle.txt")
        base_happiness_matrix (parse_happiness_matrix puzzle_input)
        dimensionality (first (shape base_happiness_matrix))
        extended_happiness_matrix (transpose (reshape (transpose (reshape base_happiness_matrix [(inc dimensionality) dimensionality])) [(inc dimensionality) (inc dimensionality)]))]
    (println extended_happiness_matrix)
    (is (= 640.0 (optimal_seating_arrangement extended_happiness_matrix)))
    )
  )
