(ns advent-of-code.aoc2015.day12-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015.day12 :refer :all])
  (:require [advent-of-code.aoc2015 :refer :all]))

;[1,2,3] and {"a":2,"b":4} both have a sum of 6.
;[[[3]]] and {"a":{"b":4},"c":-1} both have a sum of 3.
;{"a":[-1,1]} and [-1,{"a":1}] both have a sum of 0.
;[] and {} both have a sum of 0.
(deftest numbers_from_json_acceptance_test
  (is (= 6 (numbers_from_json "[1,2,3]")))
  (is (= 6 (numbers_from_json "{\"a\":2,\"b\":4}")))
  (is (= 3 (numbers_from_json "[[[3]]]")))
  (is (= 3 (numbers_from_json "{\"a\":{\"b\":4},\"c\":-1}")))
  (is (= 0 (numbers_from_json "{\"a\":[-1,1]}")))
  (is (= 0 (numbers_from_json "[-1,{\"a\":1}]")))
  (is (= 0 (numbers_from_json "[]")))
  (is (= 0 (numbers_from_json "{}")))
  )

(deftest puzzle1
  (let [puzzle_input (read-lines "resources/aoc2015/day12_puzzle1.json")]
    (is (= 191164 (numbers_from_json (nth puzzle_input 0))))
    )
  )


;[1,2,3] still has a sum of 6.
;[1,{"c":"red","b":2},3] now has a sum of 4, because the middle object is ignored.
;{"d":"red","e":[1,2,3,4],"f":5} now has a sum of 0, because the entire structure is ignored.
;[1,"red",5] has a sum of 6, because "red" in an array has no effect.
(deftest numbers_from_json_without_red_test
  (is (= 6 (numbers_from_json_without_red "[1,2,3]")))
  (is (= 4 (numbers_from_json_without_red "[1,{\"c\":\"red\",\"b\":2},3]")))
  (is (= 0 (numbers_from_json_without_red "{\"d\":\"red\",\"e\":[1,2,3,4],\"f\":5}")))
  (is (= 6 (numbers_from_json_without_red "[1,\"red\",5]")))
  )

(deftest numbers_from_json_without_red_small_tests
  (is (= 0 (numbers_from_json_without_red "[]")))
  (is (= 0 (numbers_from_json_without_red "{}")))
  (is (= 0 (numbers_from_json_without_red "0")))
  (is (= 1 (numbers_from_json_without_red "1")))
  (is (= -1 (numbers_from_json_without_red "-1")))
  (is (= 0 (numbers_from_json_without_red "\"a\"")))
  (is (= 6 (numbers_from_json_without_red "[1,2,3]")))
  (is (= 6 (numbers_from_json_without_red "{\"a\":2,\"b\":4}")))
  (is (= 7 (numbers_from_json_without_red "{\"red\":7}")))
  (is (= 0 (numbers_from_json_without_red "{\"a\":\"red\",\"b\":7}")))
  )

(deftest puzzle2
  (let [puzzle_input (read-lines "resources/aoc2015/day12_puzzle1.json")]
    (is (= 87842 (numbers_from_json_without_red (nth puzzle_input 0))))
    )
  )
