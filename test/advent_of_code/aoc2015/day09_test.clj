(ns advent-of-code.aoc2015.day09-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015 :refer :all])
  (:require [advent-of-code.aoc2015.day09 :refer :all]))

;London to Dublin = 464
;London to Belfast = 518
;Dublin to Belfast = 141
;
;The possible routes are therefore:
;
;Dublin -> London -> Belfast = 982
;London -> Dublin -> Belfast = 605
;London -> Belfast -> Dublin = 659
;Dublin -> Belfast -> London = 659
;Belfast -> Dublin -> London = 605
;Belfast -> London -> Dublin = 982
;
;The shortest of these is London -> Dublin -> Belfast = 605, and so the answer is 605 in this example.
(deftest traveling-santa-test
  (is (= 605 (traveling-santa ["London to Dublin = 464"
                               "London to Belfast = 518"
                               "Dublin to Belfast = 141"
                               ] min)))
  )

(deftest puzzle1
  (let [puzzle-lines (read-lines "resources/aoc2015/day09_puzzle1.txt")]
    (is (= 207 (traveling-santa puzzle-lines min)))
    )
  )

;For example, given the distances above, the longest route would be 982 via (for example) Dublin -> London -> Belfast.
(deftest traveling-santa-test
  (is (= 982 (traveling-santa ["London to Dublin = 464"
                               "London to Belfast = 518"
                               "Dublin to Belfast = 141"
                               ] max)))
  )

(deftest puzzle2
  (let [puzzle-lines (read-lines "resources/aoc2015/day09_puzzle1.txt")]
    (is (= 804 (traveling-santa puzzle-lines max)))
    )
  )
