(ns advent-of-code.aoc2015.day02-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015.day02 :refer :all])
  (:require [clojure.data.csv :as csv])
  (:require [clojure.java.io :as io])
  )


;A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper plus 6 square feet of slack, for a total of 58 square feet.
;A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper plus 1 square foot of slack, for a total of 43 square feet.
(deftest santas-wrapping-examples
  (is (= 58 (total-wrapping (->Present 2 3 4))))
  (is (= 43 (total-wrapping (->Present 1 1 10))))
  )

(deftest santas-wrappings-total
  (is (= 101 (total-present-wrapping (->Present 2 3 4) (->Present 1 1 10))))
  )

(defn read-Presents []
  (with-open [reader (io/reader "resources/aoc2015/day02_puzzle1.csv")]
    (let [data (csv/read-csv reader)]
      (mapv
        #(->Present (Integer/parseInt (nth % 0))
                    (Integer/parseInt (nth % 1))
                    (Integer/parseInt (nth % 2))) data)))
  )


(deftest santas-wrappings-puzzle1
  (is (= 1586300
         (->>
           (read-Presents)
           (map #(total-wrapping %))
           (reduce + 0))))
  )


;A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus 2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet.
;A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus 1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet.
(deftest satans-ribbons-examples
  (is (= 34 (total-ribbon (->Present 2 3 4))))
  (is (= 14 (total-ribbon (->Present 1 1 10))))
  )

(deftest santas-wrappings-puzzle2
  (is (= 3737498
         (->>
           (read-Presents)
           (map #(total-ribbon %))
           (reduce + 0))))
  )

