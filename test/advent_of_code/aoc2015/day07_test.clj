(ns advent-of-code.aoc2015.day07-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015 :refer :all])
  (:require [advent-of-code.aoc2015.day07 :refer :all]))

;123 -> x means that the signal 123 is provided to wire x.
;x AND y -> z means that the bitwise AND of wire x and wire y is provided to wire z.
;p LSHIFT 2 -> q means that the value from wire p is left-shifted by 2 and then provided to wire q.
;NOT e -> f means that the bitwise complement of the value from wire e is provided to wire f.
(deftest execute-program-test
  (is (= {:x 123} (execute-program ["123 -> x"])))
  (is (= {:x 255} (execute-program ["255 -> x"])))
  (is (= {:x 0} (execute-program ["0 -> x"])))
  (is (= {:x 65535} (execute-program ["65535 -> x"])))
  (is (= {:x 123 :y 255} (execute-program ["123 -> x" "255 -> y"])))

  (is (= {:e 127 :f 65408} (execute-program ["127 -> e" "NOT e -> f"])))
  (is (= {:e 65408 :f 127} (execute-program ["65408 -> e" "NOT e -> f"])))
  (is (= {:e 43690 :f 21845} (execute-program ["43690 -> e" "NOT e -> f"])))

  (is (= {:x 128 :y 127 :z 0} (execute-program ["128 -> x" "127 -> y" "x AND y -> z"])))
  (is (= {:x 64 :y 127 :z 64} (execute-program ["64 -> x" "127 -> y" "x AND y -> z"])))
  (is (= {:x 31 :y 126 :z 30} (execute-program ["31 -> x" "126 -> y" "x AND y -> z"])))

  (is (= {:x 128 :y 127 :z 255} (execute-program ["128 -> x" "127 -> y" "x OR y -> z"])))
  (is (= {:x 64 :y 127 :z 127} (execute-program ["64 -> x" "127 -> y" "x OR y -> z"])))
  (is (= {:x 31 :y 126 :z 127} (execute-program ["31 -> x" "126 -> y" "x OR y -> z"])))

  (is (= {:p 0 :q 0} (execute-program ["0 -> p" "p RSHIFT 2 -> q"])))
  (is (= {:p 127 :q 63} (execute-program ["127 -> p" "p RSHIFT 1 -> q"])))
  (is (= {:p 127 :q 31} (execute-program ["127 -> p" "p RSHIFT 2 -> q"])))
  (is (= {:p 127 :q 15} (execute-program ["127 -> p" "p RSHIFT 3 -> q"])))
  (is (= {:p 127 :q 0} (execute-program ["127 -> p" "p RSHIFT 7 -> q"])))
  (is (= {:p 127 :q 0} (execute-program ["127 -> p" "p RSHIFT 8 -> q"])))
  (is (= {:p 120 :q 15} (execute-program ["120 -> p" "p RSHIFT 3 -> q"])))

  (is (= {:p 127 :q 254} (execute-program ["127 -> p" "p LSHIFT 1 -> q"])))
  (is (= {:p 127 :q 508} (execute-program ["127 -> p" "p LSHIFT 2 -> q"])))
  (is (= {:p 127 :q 1016} (execute-program ["127 -> p" "p LSHIFT 3 -> q"])))
  (is (= {:p 127 :q 32512} (execute-program ["127 -> p" "p LSHIFT 8 -> q"])))
  (is (= {:p 127 :q 65024} (execute-program ["127 -> p" "p LSHIFT 9 -> q"])))
  (is (= {:p 127 :q 64512} (execute-program ["127 -> p" "p LSHIFT 10 -> q"])))
  (is (= {:p 127 :q 63488} (execute-program ["127 -> p" "p LSHIFT 11 -> q"])))
  (is (= {:p 127 :q 61440} (execute-program ["127 -> p" "p LSHIFT 12 -> q"])))
  (is (= {:p 127 :q 57344} (execute-program ["127 -> p" "p LSHIFT 13 -> q"])))
  (is (= {:p 127 :q 49152} (execute-program ["127 -> p" "p LSHIFT 14 -> q"])))
  (is (= {:p 127 :q 32768} (execute-program ["127 -> p" "p LSHIFT 15 -> q"])))
  (is (= {:p 127 :q 0} (execute-program ["127 -> p" "p LSHIFT 16 -> q"])))
  (is (= {:p 120 :q 960} (execute-program ["120 -> p" "p LSHIFT 3 -> q"])))
  )

(deftest execute-program-acceptance-test
  (let [expected {:d 72
                  :e 507
                  :f 492
                  :g 114
                  :h 65412
                  :i 65079
                  :x 123
                  :y 456}
        program ["123 -> x"
                 "456 -> y"
                 "x AND y -> d"
                 "x OR y -> e"
                 "x LSHIFT 2 -> f"
                 "y RSHIFT 2 -> g"
                 "NOT x -> h"
                 "NOT y -> i"]]
    (is (= expected (execute-program program)))
    (is (= 123 (execute-program program :x)))
    (is (= 456 (execute-program program :y)))
    (is (= 72 (execute-program program :d)))
    (is (= 507 (execute-program program :e)))
    (is (= 492 (execute-program program :f)))
    (is (= 114 (execute-program program :g)))
    (is (= 65412 (execute-program program :h)))
    (is (= 65079 (execute-program program :i)))
    )
  )

(deftest puzzle1-test
  (let [puzzle (read-lines "resources/aoc2015/day07_puzzle1.txt")]
    (is (= 16076 (execute-program puzzle :a)))
    )
  )

(deftest puzzle2-test
  (let [puzzle1 (read-lines "resources/aoc2015/day07_puzzle1.txt")
        puzzle2 (assoc puzzle1 54 "16076 -> b")]
    (is (= 2797 (execute-program puzzle2 :a)))
    )

  )
