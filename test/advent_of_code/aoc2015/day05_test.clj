(ns advent-of-code.aoc2015.day05-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015.day05 :refer :all])
  (:require [clojure.data.csv :as csv])
  (:require [clojure.java.io :as io])
  )


;It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
(deftest has-enough-vowels-test
  (is (not (has-enough-vowels "")))
  (is (not (has-enough-vowels "a")))
  (is (not (has-enough-vowels "aa")))
  (is (has-enough-vowels "aaa"))
  (is (not (has-enough-vowels "xxx")))
  (is (has-enough-vowels "aei"))
  (is (has-enough-vowels "xazegov"))
  (is (has-enough-vowels "aeiouaeiouaeiou"))
  )

;It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
(deftest has-double-letter-test
  (is (not (has-double-letter "")))
  (is (not (has-double-letter "a")))
  (is (not (has-double-letter "ab")))
  (is (not (has-double-letter "abcdefg")))
  (is (has-double-letter "xx"))
  (is (has-double-letter "abcdde"))
  (is (has-double-letter "aabbccdd"))
  )

;It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
(deftest does-contain-naughty-strings-test
  (is (not (does-contain-naughty-strings "")))
  (is (not (does-contain-naughty-strings "aa")))
  (is (not (does-contain-naughty-strings "xxx")))
  (is (does-contain-naughty-strings "ab"))
  (is (does-contain-naughty-strings "cd"))
  (is (does-contain-naughty-strings "pq"))
  (is (does-contain-naughty-strings "xy"))
  (is (does-contain-naughty-strings "abcdpqxy"))
  )


;ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double letter (...dd...), and none of the disallowed substrings.
;aaa is nice because it has at least three vowels and a double letter, even though the letters used by different rules overlap.
;jchzalrnumimnmhp is naughty because it has no double letter.
;haegwjzuvuyypxyu is naughty because it contains the string xy.
;dvszwmarrgswjxmb is naughty because it contains only one vowel.
(deftest nice-tes
  (is (nice? "ugknbfddgicrmopn"))
  (is (nice? "aaa"))
  (is (not (nice? "jchzalrnumimnmhp")))
  (is (not (nice? "haegwjzuvuyypxyu")))
  (is (not (nice? "dvszwmarrgswjxmb")))
  )

(defn read-nice-naughty-list []
  (with-open [reader (io/reader "resources/aoc2015/day05_puzzle1.csv")]
    (let [data (csv/read-csv reader)]
      (mapv #(nth % 0) data)
      )
    )
  )

(deftest day05-puzzle1
  (is (= 255
         (->>
           (read-nice-naughty-list)
           (map #(if (nice? %)
                   1
                   0))
           (reduce + 0))))
  )


;It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
(deftest contains-non-overlapping-repeating-pair-test
  (is (not (contains-non-overlapping-repeating-pair "")))
  (is (not (contains-non-overlapping-repeating-pair "a")))
  (is (not (contains-non-overlapping-repeating-pair "aa")))
  (is (contains-non-overlapping-repeating-pair "xyxy"))
  (is (contains-non-overlapping-repeating-pair "aabcdefgaa"))
  (is (not (contains-non-overlapping-repeating-pair "aaa")))
  )

;It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
(deftest has-repeating-letter-with-spacer-test
  (is (not (has-repeating-letter-with-spacer "")))
  (is (not (has-repeating-letter-with-spacer "a")))
  (is (not (has-repeating-letter-with-spacer "aa")))
  (is (has-repeating-letter-with-spacer "xyx"))
  (is (has-repeating-letter-with-spacer "abcdefeghi"))
  (is (has-repeating-letter-with-spacer "aaa"))
  )

;qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that repeats with exactly one letter between them (zxz).
;xxyxx is nice because it has a pair that appears twice and a letter that repeats with one between, even though the letters used by each rule overlap.
;uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single letter between them.
;ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo), but no pair that appears twice.
(deftest nice2-test
  (is (nice2? "qjhvhtzxzqqjkmpb"))
  (is (nice2? "xxyxx"))
  (is (not (nice2? "uurcxstgmygtbstg")))
  (is (not (nice2? "ieodomkazucvgmuy")))
  )


(deftest day05-puzzle2
  (is (= 55
         (->>
           (read-nice-naughty-list)
           (map #(if (nice2? %)
                   1
                   0))
           (reduce + 0))))
  )