(ns advent-of-code.aoc2015.day08-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015 :refer :all])
  (:require [advent-of-code.aoc2015.day08 :refer :all]))


;"" is 2 characters of code (the two double quotes), but the string contains zero characters.
;"abc" is 5 characters of code, but 3 characters in the string data.
;"aaa\"aaa" is 10 characters of code, but the string itself contains six "a" characters and a single, escaped quote character, for a total of 7 characters in the string data.
;"\x27" is 6 characters of code, but the string itself contains just one - an apostrophe ('), escaped using hexadecimal notation.
(deftest single-words
  (is (= 0 (characters-in-string "\"\"")))
  (is (= 3 (characters-in-string "\"abc\"")))
  (is (= 7 (characters-in-string "\"aaa\\\"aaa\"")))
  (is (= 1 (characters-in-string "\\x27\"")))
  (is (= 1 (characters-in-string "\"\\\\\"")))
  )

;For example, given the four strings above, the total number of characters of string code (2 + 5 + 10 + 6 = 23)
;minus the total number of characters in memory for string values (0 + 3 + 7 + 1 = 11) is 23 - 11 = 12.
(deftest word-list
  (is (= 12 (character-offset ["\"\"" "\"abc\"" "\"aaa\\\"aaa\"" "\"\\x27\""])))
  )


(deftest puzzle1
  (let [puzzle (read-lines "resources/aoc2015/day08_puzzle1.txt")]
    (is (= 1350 (character-offset puzzle)))
    )
  )


;"" encodes to "\"\"", an increase from 2 characters to 6.
;"abc" encodes to "\"abc\"", an increase from 5 characters to 9.
;"aaa\"aaa" encodes to "\"aaa\\\"aaa\"", an increase from 10 characters to 16.
;"\x27" encodes to "\"\\x27\"", an increase from 6 characters to 11.
(deftest single-words-reverse
  (is (= 6 (encoded-characters-in-string "\"\"")))
  (is (= 9 (encoded-characters-in-string "\"abc\"")))
  (is (= 16 (encoded-characters-in-string "\"aaa\\\"aaa\"")))
  (is (= 11 (encoded-characters-in-string "\"\\x27\"")))
  (is (= 10 (encoded-characters-in-string "\"\\\\\"")))
  )

;Your task is to find the total number of characters to represent the newly encoded strings minus the number of
;characters of code in each original string literal. For example, for the strings above, the total encoded length
;(6 + 9 + 16 + 11 = 42) minus the characters in the original code representation (23, just like in the first part of
;this puzzle) is 42 - 23 = 19.
(deftest word-list-reverse
    (is (= 19 (encoded-character-offset ["\"\"" "\"abc\"" "\"aaa\\\"aaa\"" "\"\\x27\""])))
  )


(deftest puzzle2
  (let [puzzle (read-lines "resources/aoc2015/day08_puzzle1.txt")]
    (is (= 2085 (encoded-character-offset puzzle)))
    )
  )
