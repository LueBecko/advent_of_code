(ns advent-of-code.aoc2015.day10-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015.day10 :refer :all]))

;1 becomes 11 (1 copy of digit 1).
;11 becomes 21 (2 copies of digit 1).
;21 becomes 1211 (one 2 followed by one 1).
;1211 becomes 111221 (one 1, one 2, and two 1s).
;111221 becomes 312211 (three 1s, two 2s, and one 1).
(deftest look-and-say-test
  (is (= "11" (look-and-say "1")))
  (is (= "21" (look-and-say "11")))
  (is (= "1211" (look-and-say "21")))
  (is (= "111221" (look-and-say "1211")))
  (is (= "312211" (look-and-say "111221")))
  (is (= "22" (look-and-say "22")))
  )

(deftest look-and-say-repetitive-test
  (is (= "11" (look-and-say "1" 1)))
  (is (= "21" (look-and-say "1" 2)))
  (is (= "1211" (look-and-say "1" 3)))
  (is (= "111221" (look-and-say "1" 4)))
  (is (= "312211" (look-and-say "1" 5)))
  (is (= "22" (look-and-say "22" 50)))
  )

(deftest puzzle1
  (is (= 360154 (count (look-and-say "1113122113" 40))))
  )

(deftest puzzle2
  (is (= 5103798 (count (look-and-say "1113122113" 50))))
  )
