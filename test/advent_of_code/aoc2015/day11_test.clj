(ns advent-of-code.aoc2015.day11-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015.day11 :refer :all]))

(deftest valid_password_test_length_restriction
  (is (not (valid_password? "")))
  (is (not (valid_password? "abc")))
  (is (not (valid_password? "aabcdef")))
  (is (not (valid_password? "aabcdefff")))
  (is (valid_password? "aabcdeff"))
  )

(deftest valid_password_test_lowercase_letter_restriction
  (is (valid_password? "aabcdeff"))
  (is (not (valid_password? "aabcdeffA")))
  (is (not (valid_password? "aabcdeff ")))
  (is (not (valid_password? "aabcdeff1")))
  (is (not (valid_password? "aabcdeff+")))
  )

; new policies start here
(deftest valid_password_test_increasing_straight_restriction
  (is (valid_password? "aabcdeff"))
  (is (not (valid_password? "ababbaab")))
  (is (not (valid_password? "aaaaaaaa")))
  (is (not (valid_password? "abdddddd")))
  )

(deftest valid_password_test_without_confusing_characters_restriction
  (is (valid_password? "aabcdeff"))
  (is (not (valid_password? "abcdefgi")))
  (is (not (valid_password? "abcdefgo")))
  (is (not (valid_password? "abcdefgl")))
  )

(deftest valid_password_test_two_non_overlapping_pairs_restriction
  (is (valid_password? "aabcdeff"))
  (is (valid_password? "aabcccff"))
  (is (valid_password? "aaaabcde"))                         ; non-overlapping, but identical pairs
  (is (not (valid_password? "aaabcdef")))                   ; overlapping aaa
  (is (not (valid_password? "aabcdefg")))                   ; only one pair
  )

;hijklmmn meets the first requirement (because it contains the straight hij) but fails the second requirement requirement (because it contains i and l).
;abbceffg meets the third requirement (because it repeats bb and ff) but fails the first requirement.
;abbcegjk fails the third requirement, because it only has one double letter (bb).
(deftest acceptance_tests
  (is (not (valid_password? "hijklmmn")))
  (is (not (valid_password? "abbceffg")))
  (is (not (valid_password? "abbcegjk")))
  )

;The next password after abcdefgh is abcdffaa.
;The next password after ghijklmn is ghjaabcc, because you eventually skip all the passwords that start with ghi..., since i is not allowed.
(deftest santas_next_password_acceptance
  (is (= "abcdffaa" (santas_next_password "abcdefgh")))
  (is (= "ghjaabcc" (santas_next_password "ghijklmn")))
  (is (= "aaaaaabc" (santas_next_password "zzzzzzzz")))
  )

(deftest puzzle1
  (is (= "hepxxyzz" (santas_next_password "hepxcrrq")))
  )

(deftest puzzle2
  (is (= "heqaabcc" (santas_next_password (santas_next_password "hepxcrrq"))))
  )

