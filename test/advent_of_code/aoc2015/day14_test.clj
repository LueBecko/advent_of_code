(ns advent-of-code.aoc2015.day14-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015 :refer :all])
  (:require [advent-of-code.aoc2015.day14 :refer :all]
            [clojure.string :as str]))


;Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
;Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
;In this example, after the 1000th second, both reindeer are resting,
; and Comet is in the lead at 1120 km (poor Dancer has only gotten 1056 km by that point).
(deftest race-distance_of_reindeer_acceptance_test
  (is (= 1120 (race-distance (->Reindeer 14 10 127) 1000)))
  (is (= 1056 (race-distance (->Reindeer 16 11 162) 1000)))
  )

(deftest race-distance_of_reindeer_acceptance_test
  (is (= 0 (race-distance (->Reindeer 2 10 5) 0)))
  (is (= 2 (race-distance (->Reindeer 2 10 5) 1)))
  (is (= 4 (race-distance (->Reindeer 2 10 5) 2)))
  (is (= 18 (race-distance (->Reindeer 2 10 5) 9)))
  (is (= 20 (race-distance (->Reindeer 2 10 5) 10)))
  (is (= 20 (race-distance (->Reindeer 2 10 5) 11)))
  (is (= 20 (race-distance (->Reindeer 2 10 5) 12)))
  (is (= 20 (race-distance (->Reindeer 2 10 5) 15)))
  (is (= 22 (race-distance (->Reindeer 2 10 5) 16)))
  (is (= 24 (race-distance (->Reindeer 2 10 5) 17)))
  (is (= 38 (race-distance (->Reindeer 2 10 5) 24)))
  (is (= 40 (race-distance (->Reindeer 2 10 5) 25)))
  (is (= 40 (race-distance (->Reindeer 2 10 5) 26)))
  (is (= 40 (race-distance (->Reindeer 2 10 5) 30)))
  (is (= 42 (race-distance (->Reindeer 2 10 5) 31)))
  )

(deftest puzzle1
  (let [puzzle-data (read-lines "resources/aoc2015/day14_puzzle1.txt")
        preprocessed-puzzle-data (mapv (fn [line] (str/split line #" ")) puzzle-data)
        reindeers (mapv (fn [line] (->Reindeer (parse-int (nth line 3)) (parse-int (nth line 6)) (parse-int (nth line 13)))) preprocessed-puzzle-data)]
    (is (= 2660 (reduce max (evaluate-race reindeers 2503))))
    )
  )

(deftest puzzle2
  (let [puzzle-data (read-lines "resources/aoc2015/day14_puzzle1.txt")
        preprocessed-puzzle-data (mapv (fn [line] (str/split line #" ")) puzzle-data)
        reindeers (mapv (fn [line] (->Reindeer (parse-int (nth line 3)) (parse-int (nth line 6)) (parse-int (nth line 13)))) preprocessed-puzzle-data)]
    (is (= 1256 (reduce max (point-race reindeers 2503))))
    )
  )
