(ns advent-of-code.aoc2015.day04-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.aoc2015.day04 :refer [generate-hash-start produce-amendedSecret mine-AdventCoin compute-hash-start]]))

(deftest generate-hash-start-test
  (is (= "" (generate-hash-start 0)))
  (is (= "0" (generate-hash-start 1)))
  (is (= "00" (generate-hash-start 2)))
  (is (= "000" (generate-hash-start 3)))
  (is (= "0000" (generate-hash-start 4)))
  (is (= "00000" (generate-hash-start 5)))
  )

(deftest produce-amendedSecret-test
  (is (= "abcdef0" (produce-amendedSecret "abcdef" 0)))
  (is (= "abcdef5" (produce-amendedSecret "abcdef" 5)))
  (is (= "abcdef35" (produce-amendedSecret "abcdef" 35)))
  (is (= "abcdef8765" (produce-amendedSecret "abcdef" 8765)))
  )

(deftest compute-hash-start-test
  (is (= (generate-hash-start 5) (compute-hash-start "abcdef" 5 609043)))
  )

;If your secret key is abcdef, the answer is 609043, because the MD5 hash of abcdef609043 starts with five zeroes (000001dbbfa...), and it is the lowest such number to do so.
;If your secret key is pqrstuv, the lowest number it combines with to make an MD5 hash starting with five zeroes is 1048970; that is, the MD5 hash of pqrstuv1048970 looks like 000006136ef....
(deftest acceptance-tests
  (is (=  609043 (mine-AdventCoin "abcdef"  5)))
  (is (= 1048970 (mine-AdventCoin "pqrstuv" 5)))
  )

(deftest mine-AdventCoin-puzzle1
  (is (= 117946 (mine-AdventCoin "ckczppom" 5)))
  )

(deftest mine-AdventCoin-puzzle2
  (is (= 3938038 (mine-AdventCoin "ckczppom" 6)))
  )