(ns advent-of-code.aoc2015.day10)

(defn- ^CharSequence convert-part [^CharSequence part]
  (str (count part) (first part))
  )

(defn ^CharSequence look-and-say
  ([^CharSequence sequence]
   (->> sequence
        (partition-by identity)
        (map convert-part)
        (reduce str))
   )
  ([^CharSequence sequence ^Integer steps]
   (println "step " steps ": length " (count sequence))
   (if (= steps 0)
     sequence
     (look-and-say (look-and-say sequence) (dec steps))
     )
   )
  )