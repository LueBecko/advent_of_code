(ns advent-of-code.aoc2015.day07
  (:require [clojure.string :as str]
            [advent-of-code.aoc2015 :refer :all]
            )
  )

(defrecord Instruction [in-symbols in-values operator out-symbol out-value])

; return vector of symbols
(defn- parse-symbols [operands]
  (map keyword (filter (fn [op] (not (nil? (re-matches #"^[a-z]*$" op)))) operands))
  )

; return vector of values
(defn- parse-values [operands]
  (map parse-int (filter (fn [op] (not (nil? (re-matches #"^\d*$" op)))) operands))
  )

(defn- operation-assign [^Instruction line other-lines]
  (let [input-symbols (:in-symbols line)
        input-value (if (empty? input-symbols)
                      (nth (:in-values line) 0)
                      (:out-value (get other-lines (nth input-symbols 0)))
                      )]
    (Instruction. (:in-symbols line) (:in-values line) (:operator line) (:out-symbol line) input-value)
    )
  )

(defn- operation-not [^Instruction line other-lines]
  (let [input-symbol (nth (:in-symbols line) 0)
        input-value (:out-value (get other-lines input-symbol))]
    (Instruction. (:in-symbols line) (:in-values line) (:operator line) (:out-symbol line) (->> input-value
                                                                                                bit-not
                                                                                                (bit-and 16rFFFF)))
    )
  )

(defn- operation-shift [^Instruction line other-lines operator]
  (let [input-value (:out-value (get other-lines (nth (:in-symbols line) 0)))
        shifter (fn [val] (operator val (nth (:in-values line) 0)))]
    (Instruction. (:in-symbols line) (:in-values line) (:operator line) (:out-symbol line) (->> input-value
                                                                                                shifter
                                                                                                (bit-and 16rFFFF)))
    )
  )

(defn- operation-lshift [^Instruction line other-lines]
  (operation-shift line other-lines bit-shift-left)
  )

(defn- operation-rshift [^Instruction line other-lines]
  (operation-shift line other-lines bit-shift-right)
  )


(defn- operation-and [^Instruction line memory]
  (let [in-symbols (:in-symbols line)
        input-valueA (:out-value (get memory (nth in-symbols 0)))
        input-valueB (if (= 1 (count in-symbols))
                       (nth (:in-values line) 0)
                       (:out-value (get memory (nth in-symbols 1))))]
    (Instruction. (:in-symbols line) (:in-values line) (:operator line) (:out-symbol line) (bit-and input-valueA input-valueB))
    )
  )

(defn- operation-or [^Instruction line other-lines]
  (let [input-valueA (:out-value (get other-lines (nth (:in-symbols line) 0)))
        input-valueB (:out-value (get other-lines (nth (:in-symbols line) 1)))]
    (Instruction. (:in-symbols line) (:in-values line) (:operator line) (:out-symbol line) (bit-or input-valueA input-valueB))
    )
  )

(defn- operator-regex [operator]
  (case operator
    operation-not #"NOT"
    operation-lshift #"LSHIFT"
    operation-rshift #"RSHIFT"
    operation-and #"AND"
    operation-or #"OR"
    #" "
    )
  )

(defn- map-operator [op-part]
  (cond (str/starts-with? op-part "NOT") operation-not
        (str/includes? op-part "LSHIFT") operation-lshift
        (str/includes? op-part "RSHIFT") operation-rshift
        (str/includes? op-part "AND") operation-and
        (str/includes? op-part "OR") operation-or
        :else operation-assign
        )
  )

(defn- ^Instruction parse-instruction [^String operation]
  (let [parts (str/split operation #"->")
        out-symbol (keyword (str/trim (nth parts 1)))
        op-part (str/trim (nth parts 0))
        operator (map-operator op-part)
        operands (map str/trim (str/split op-part (operator-regex operator)))
        in-symbols (parse-symbols operands)
        in-values (parse-values operands)
        ]
    (Instruction. in-symbols in-values operator out-symbol nil)
    )
  )

(defn- parse-instructions [lines]
  (let [parsed-instructions (map parse-instruction lines)]
    (zipmap (map (fn [^Instruction op] (:out-symbol op)) parsed-instructions) parsed-instructions)
    )
  )

(defn- executable? [^Instruction instruction instructions]
  (and (nil? (:out-value instruction))
       (not-any? (fn [symbol] (nil? (:out-value (get instructions symbol)))) (:in-symbols instruction))
       )
  )

(defn- execute-instruction [^Instruction instruction all-instructions]
  ((:operator instruction) instruction all-instructions)
  )

(defn- update-instructions [executable-instructions all-instructions]
  (merge all-instructions
         (zipmap (map :out-symbol executable-instructions)
                 (map #(execute-instruction % all-instructions) executable-instructions)
                 )
         )
  )

(defn- execute-instructions [instructions]
  (let [executable-instructions (filter (fn [instruction] (executable? instruction instructions)) (vals instructions))
        done (empty? executable-instructions)]
    (if done
      instructions
      (let [updated-instructions (update-instructions executable-instructions instructions)]
        (execute-instructions updated-instructions)
        )
      )
    )
  )

(defn execute-program
  ([raw-instructions]
   (let [instructions (parse-instructions raw-instructions)
         executed-instructions (execute-instructions instructions)]
     (zipmap (map :out-symbol (vals instructions))
             (map :out-value (vals executed-instructions)))
     )
   )
  ([raw-instructions target-symbol]
   (get (execute-program raw-instructions) target-symbol)
   )
  )
