(ns advent-of-code.aoc2015.day13
  (:require [advent-of-code.aoc2015 :refer :all])
  (:refer-clojure :exclude [* - + / == not= min max])
  (:require clojure.core.matrix.impl.ndarray
            [clojure.core.matrix :refer :all]
            [clojure.core.matrix.operators :refer :all])
  (:require [clojure.math.combinatorics :as combo]
            [clojure.string :as str])
  )

(defn- extend-incomplete-row [row_index row]
  (into (into
          (subvec row 0 row_index)
          [0])
        (subvec row row_index (count row)))
  )

(defn parse_happiness_matrix [happiness_data]
  (let [split-happiness-data (map (fn [line] (str/split (str/replace line "." "") #" ")) happiness_data)
        dimensionality (count (distinct (map (fn [line] (nth line 0)) split-happiness-data)))
        happiness (map (fn [line] (* (if (= "lose" (nth line 2)) -1 1) (parse-int (nth line 3)))) split-happiness-data)
        pre-happiness-matrix (reshape happiness [dimensionality (dec dimensionality)])
        ]
    (mapv (fn [row_index] (extend-incomplete-row row_index (get pre-happiness-matrix row_index)))
          (range 0 dimensionality))
    )
  )

(defn- generate-seating-row [row number_of_guests]
  (let [base_row (zero-vector number_of_guests)
        before_diag_row (if (> row 0) (mset base_row (dec row) 1) (mset base_row (dec number_of_guests) 1))
        after_diag_row (if (< row (dec number_of_guests)) (mset before_diag_row (inc row) 1) (mset before_diag_row 0 1))
        ]
    after_diag_row
    )
  )

(defn- generate-seating-matrix [number_of_guests]
  (mapv (fn [row] (generate-seating-row row number_of_guests)) (range 0 number_of_guests))
  )

(defn- generate-permutation-matrix [index range]
  (permutation-matrix (conj (combo/nth-permutation range index) (count range)))
  )

(defn- permutate-seatings [seating-matrix perm_index perm_range]
  (let [perm-matrix (generate-permutation-matrix perm_index perm_range)]
    (mmul perm-matrix (mmul seating-matrix (transpose perm-matrix)))
    )
  )

(defn ^Integer optimal_seating_arrangement [happiness_matrix]
  (let [number_of_guests (nth (shape happiness_matrix) 0)
        permutation_range (range 0 (dec number_of_guests))
        possible_arrangements (combo/count-permutations permutation_range) ; note: circular seating reduces the number of degrees of freedom by 1
        seating_matrix (generate-seating-matrix number_of_guests)
        ]
    ; solution: iterate over all possible permutations and evaluate each permutation - return the best
    (reduce max (map (fn [arrangement_index] (esum
                                               (mul happiness_matrix
                                                    (permutate-seatings seating_matrix arrangement_index permutation_range)
                                                    )))
                     (range 0 possible_arrangements)))
    )
  )

