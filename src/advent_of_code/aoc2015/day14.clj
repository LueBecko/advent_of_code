(ns advent-of-code.aoc2015.day14
  (:refer-clojure :exclude [* - + / == not= min max])
  (:require clojure.core.matrix.impl.ndarray
            [clojure.core.matrix :refer :all]
            [clojure.core.matrix.operators :refer :all])
  )

(defprotocol ReindeerProtocol
  (race-distance [this race-duration] "how far does the reindeer run in a given race duration")
  )

(deftype Reindeer [speed duration rest]
  ReindeerProtocol
  (race-distance [this race-duration]
    (+
      (* (* speed duration)
         (quot race-duration (+ duration rest))
         )
      (* (min duration (rem race-duration (+ duration rest)))
         speed)
      ))
  )

(defn evaluate-race [reindeers race-duration]
  (map (fn [reindeer] (race-distance reindeer race-duration)) reindeers)
  )

(defn- updated-points [reindeers timepoint points]
  (let [race-result (evaluate-race reindeers timepoint)
        winner-distance (reduce max race-result)
        winner-vector (map (fn [distance] (if (= distance winner-distance) 1 0)) race-result)
        ]
    (add points winner-vector)
    )
  )

(defn point-race [reindeers race-duration]

  (loop [timepoint 1
         points (repeat (count reindeers) 0) ]
    (if (= timepoint race-duration)
      points
      (recur (inc timepoint) (updated-points reindeers timepoint points))
      )
    )

  )

