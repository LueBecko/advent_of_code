(ns advent-of-code.aoc2015.day03)

(defprotocol CoordinateBehaviour (add [this that]))
(defrecord coordinate [x y]
  CoordinateBehaviour
  (add [this that] (->coordinate (+ (.x this) (.x that)) (+ (.y this) (.y that))))
  )

(defn- translate-instruction [instruction]
  (cond
    (= \^ instruction) (->coordinate 0 1)
    (= \v instruction) (->coordinate 0 -1)
    (= \< instruction) (->coordinate -1 0)
    (= \> instruction) (->coordinate 1 0)
    :else (->coordinate 0 0)
    )
  )

(defn- compute-coordinate-sequence [instructions]
  (if (empty? instructions)
    [(->coordinate 0 0)]
    (cons (->coordinate 0 0)
          (reductions add (map translate-instruction instructions)))
    )
  )

(defn visited-houses [instructions]
  (count (distinct (compute-coordinate-sequence instructions)))
  )

; returns a set of two instruction strings (even instructions, odd instructions)
(defn- split-instructions [instructions]
  [
   (map #(get instructions %) (filter even? (range (count instructions))))
   (map #(get instructions %) (filter odd? (range (count instructions))))
   ]
  )

(defn visited-houses-with-robot-santa [instructions]
  (count (distinct
           (flatten                                         ;concat?
             (map compute-coordinate-sequence (split-instructions instructions)))
           ))
  )
