(ns advent-of-code.aoc2015.day12
  (:require [clojure.data.json :as json])
  (:require [advent-of-code.aoc2015 :refer :all]))

(defn- ^Integer calculate_numbers_sum [object]
  (cond
    (string? object) 0
    (int? object) object
    (empty? object) 0
    :else (reduce + 0 (map calculate_numbers_sum object))
    )
  )

(defn ^Integer numbers_from_json [json]
  (calculate_numbers_sum (json/read-str json))
  )

(defn ^Boolean has_red [map_object]
  (not (not-any? (fn [val] (= val "red")) (vals map_object)))
  )

(defn- ^Integer calculate_numbers_sum_without_red [object]
  (cond
    (int? object) object
    (string? object) 0
    (empty? object) 0
    (and (map? object) (has_red object)) 0
    :else (reduce + 0 (map calculate_numbers_sum_without_red object))
    )
  )

(defn numbers_from_json_without_red [json]
  (calculate_numbers_sum_without_red (json/read-str json))
  )


