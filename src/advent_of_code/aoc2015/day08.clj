(ns advent-of-code.aoc2015.day08
  (:require [clojure.string :as str]))

(defn characters-in-string [^String string]
  (let [transformed-quote-literal (str/replace string "\\\"" "\"")
        transformed-opening-quotes (str/replace transformed-quote-literal #"^\"" "")
        transformed-closing-quotes (str/replace transformed-opening-quotes #"\"$" "")
        transformed-hex-literal (str/replace transformed-closing-quotes #"\\x[0-9a-f]{2}" " ")
        transformed-escape-literal (str/replace transformed-hex-literal "\\\\" "\\")]
    (count transformed-escape-literal)
    )
  )

(defn character-offset [strings]
  (let [all-length (reduce + 0 (map count strings))
        all-characters-in-string (reduce + 0 (map characters-in-string strings))]
    (- all-length all-characters-in-string)
    )
  )

(defn encoded-characters-in-string [^String string]
  (let [encode-escape (str/replace string "\\" "\\\\")
        encode-quotes (str/replace encode-escape "\"" "\\\"")]
    (+ 2 (count encode-quotes))
    )
  )

(defn encoded-character-offset [strings]
  (let [all-length (reduce + 0 (map count strings))
        all-characters-in-string (reduce + 0 (map encoded-characters-in-string strings))]
    (- all-characters-in-string all-length)
    )
  )

