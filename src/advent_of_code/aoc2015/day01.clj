(ns advent-of-code.aoc2015.day01)

(defn- translate-instruction [instruction]
  (cond (= instruction \)) (- 0 1)
        (= instruction \() 1
        :else 0)
  )

(defn santas-floor [instructions]

  (reduce + (map translate-instruction instructions))

  ;(cond (= 0 (count instructions)) 0
  ;      (= 1 (count instructions)) (translate-instruction (get instructions 0))
  ;      :else
  ;  (+ (santas-floor (subs instructions 0 (quot (count instructions) 2)))
  ;     (santas-floor (subs instructions (quot (count instructions) 2)))))

  )

(defn- find-first-index [f coll]
  (+ 1 (reduce min (keep-indexed #(if (f %2) %1) coll)))
  )

(defn santas-first-basement-visit [instructions]
  (find-first-index (fn [floor] (= (- 0 1) floor))
                    (reductions + (map translate-instruction instructions)))
  )
