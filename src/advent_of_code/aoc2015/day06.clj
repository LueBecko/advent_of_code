(ns advent-of-code.aoc2015.day06
  (:refer-clojure :exclude [* - + / == not= = min max])
  (:require clojure.core.matrix.impl.ndarray
            [clojure.core.matrix :refer :all]
            [clojure.core.matrix.operators :refer :all]
            [clojure.string :as str]
            [advent-of-code.aoc2015 :refer :all]
            ))

(defn- is-in-range [index upper lower left right]
  (and
    (>= (nth index 0) upper)
    (<= (nth index 0) lower)
    (>= (nth index 1) left)
    (<= (nth index 1) right))
  )

(defprotocol Grid
  (evaluate [this])
  (get-size [this])
  (turn-on [this upper left lower right])
  (turn-off [this upper left lower right])
  (toggle [this upper left lower right])
  )

(defrecord GridBoolean [grid]
  Grid
  (get-size [this] (shape (.grid this)))
  (evaluate [this]
    (esum (emap (fn [b] (if b 1 0)) (.grid this)))
    )
  (turn-on [this upper left lower right]
    (->GridBoolean
      (emap-indexed
        (fn [index val]
          (if (is-in-range index upper lower left right)
            Boolean/TRUE
            val
            ))
        (.grid this))
      )
    )
  (turn-off [this upper left lower right]
    (->GridBoolean
      (emap-indexed
        (fn [index val]
          (if (is-in-range index upper lower left right)
            Boolean/FALSE
            val
            ))
        (.grid this))
      )
    )
  (toggle [this upper left lower right]
    (->GridBoolean
      (emap-indexed
        (fn [index val]
          (if (is-in-range index upper lower left right)
            (not val)
            val
            ))
        (.grid this))
      )
    )
  )

(defn generate-matrix [size upper left lower right ^Integer innerval ^Integer outerval]
  (let [base-matrix (new-matrix (nth size 0) (nth size 1))]
    (emap-indexed
      (fn [index val]
        (if (is-in-range index upper lower left right)
          innerval
          outerval
          ))
      base-matrix)
    )
  )

(defrecord GridBrightness [grid]
  Grid
  (get-size [this] (shape (.grid this)))
  (evaluate [this]
    (int (esum (.grid this)))
    )
  (turn-on [this upper left lower right]
    (->GridBrightness
      (add (.grid this) (generate-matrix (get-size this) upper left lower right 1 0))
      )
    )
  (turn-off [this upper left lower right]
    (->GridBrightness
      (emap #(max 0 %)
            (sub (.grid this) (generate-matrix (get-size this) upper left lower right 1 0))
            )
      )
    )
  (toggle [this upper left lower right]
    (->GridBrightness
      (add (.grid this) (generate-matrix (get-size this) upper left lower right 2 0))
      )
    )
  )

(defn ^GridBoolean initialise-grid [^long h ^long w]
  (->GridBoolean (make-array Boolean/TYPE h w))
  )

(defn ^GridBrightness initialise-brightness-grid [^long h ^long w]
  (->GridBrightness (new-matrix h w))
  )

(defrecord Instruction [method upper left lower right])

(defn ^Instruction parse-instruction [^String instruction]
  (let [method (cond (str/starts-with? instruction "turn on") turn-on
                     (str/starts-with? instruction "turn off") turn-off
                     (str/starts-with? instruction "toggle") toggle
                     )
        stripped-instruction (cond (str/starts-with? instruction "turn on") (subs instruction 8)
                                   (str/starts-with? instruction "turn off") (subs instruction 9)
                                   (str/starts-with? instruction "toggle") (subs instruction 7))
        coordinate-instruction (str/replace stripped-instruction " through " ",")
        coordinates (map parse-int (str/split coordinate-instruction #","))
        ]
    (->Instruction method (nth coordinates 0) (nth coordinates 1) (nth coordinates 2) (nth coordinates 3))
    )
  )

(defn follow-santas-instruction [grid ^String instruction]
  (let [instr (parse-instruction instruction)]
    ((.method instr) grid (.upper instr) (.left instr) (.lower instr) (.right instr))
    )
  )
