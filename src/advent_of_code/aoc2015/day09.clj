(ns advent-of-code.aoc2015.day09
  (:require [advent-of-code.aoc2015 :as aoc2015])
  (:require [clojure.string :as str]))

; does the actual TSP computation in full recursion
(defn ^Integer TSP [start-city other-cities get-distance target-function]
  (let [n-cities (count other-cities)]
    (if (= 1 n-cities)
      (get-distance start-city (first other-cities))

      (reduce target-function
              (map (fn [other-city]
                     (+ (get-distance start-city other-city)
                        (TSP other-city (disj other-cities other-city) get-distance target-function)))
                   other-cities))
      )
    )

  )

(defn- parse-distance-matrix [raw-distances]
  (let [raw-distance-pairs (map (fn [line] (str/split line #" = ")) raw-distances)
        city-pairs (map (fn [pair] (set (str/split (nth pair 0) #" to "))) raw-distance-pairs)
        distances (map (fn [pair] (aoc2015/parse-int (nth pair 1))) raw-distance-pairs)
        ]
    (zipmap city-pairs distances)
    )
  )

(defn ^Integer traveling-santa [distances target-function]
  (let [distance-map (parse-distance-matrix distances)
        cities (reduce clojure.set/union (keys distance-map))
        get-distance (fn [city other-city]
                       (get distance-map #{city other-city} 0))
        ]
    (reduce target-function
            (map
              (fn [start-city] (TSP start-city (disj cities start-city) get-distance target-function))
              cities))
    ;605
    )
  )
