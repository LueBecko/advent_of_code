(ns advent-of-code.aoc2015.day05)

(defn ^Boolean has-enough-vowels [^String input]
  (>= (count (clojure.string/replace input #"[^aeiou]" "")) 3)
  )

(defn ^Boolean has-double-letter [^String input]
  (not (empty? (re-find #"(.)\1+" input)))
  )

(defn ^Boolean does-contain-naughty-strings [^String input]
  (not (empty? (re-find #"(ab)|(cd)|(pq)|(xy)" input)))
  )

(defn ^Boolean nice? [^String input]
  (and
    (has-enough-vowels input)
    (has-double-letter input)
    (not (does-contain-naughty-strings input))
    )
  )

(defn ^Boolean contains-non-overlapping-repeating-pair [^String input]
  (not (empty? (re-find #"(..).*\1+" input)))
  )

(defn ^Boolean has-repeating-letter-with-spacer [^String input]
  (not (empty? (re-find #"(.).\1+" input)))
  )

(defn ^Boolean nice2? [^String input]
  (and
    (contains-non-overlapping-repeating-pair input)
    (has-repeating-letter-with-spacer input)
    )
  )
