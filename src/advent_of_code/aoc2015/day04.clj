(ns advent-of-code.aoc2015.day04)
(import 'java.security.MessageDigest
        'java.math.BigInteger)

(defn ^String md5 [^String s]
  (let [algorithm (MessageDigest/getInstance "MD5")
        raw (.digest algorithm (.getBytes s))]
    (format "%032x" (BigInteger. 1 raw))))

(defn ^String generate-hash-start [^Integer complexity]
  (clojure.string/join "" (repeat complexity "0"))
  )

(defn ^String produce-amendedSecret [^String secret ^Long cnt]
  (clojure.string/join "" [secret (.toString cnt)])
  )

(defn ^String compute-hash-start [^String secret ^Long complexity ^Long cnt]
  (subs (md5 (produce-amendedSecret secret cnt)) 0 complexity)
  )

(defn ^Long mine-AdventCoin [^String secret ^Long complexity]
  ; Recursive solution fails on stack overflow :(
  ;(if (=
  ;      (generate-hash-start complexity)
  ;      (subs (md5 (produce-amendedSecret secret cnt)) complexity))
  ;  cnt
  ;  (mine-AdventCoin secret complexity (inc cnt))
  ;  )

  (loop [c 0]
    (if (=
          (generate-hash-start complexity)
          (compute-hash-start secret complexity c))
      c
      (recur (inc c))
      )
    )
  )

