(ns advent-of-code.aoc2015.day02)

(defprotocol Wrapping
  (smallest-side [this])
  (total-wrapping [this])
  (smallest-perimeter [this])
  (total-ribbon [this]))

(defrecord Present [^int l ^int w ^int h]
  Wrapping
  (smallest-side [this]
    (min (* (.l this) (.w this)) (* (.l this) (.h this)) (* (.w this) (.h this)))
    )
  (total-wrapping [this]
    (+
      (* 2 (+ (* (.l this) (.w this)) (* (.l this) (.h this)) (* (.w this) (.h this))))
      (smallest-side this)
      )
    )
  (total-ribbon [this]
    (+ (* (.l this) (.w this) (.h this))
       (smallest-perimeter this)
       ))
  (smallest-perimeter [this]
    (* 2
       (- (+ (.l this) (.w this) (.h this))
          (max (.l this) (.w this) (.h this)))
       ))
  )

(defn total-present-wrapping [& presents]
  (apply + (map total-wrapping presents))
  )
