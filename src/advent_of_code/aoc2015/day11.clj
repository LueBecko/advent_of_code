(ns advent-of-code.aoc2015.day11)

; Corporate policy dictates that passwords must be exactly eight lowercase letters (for security reasons)
(defn- ^Boolean validate-base-password-format [^CharSequence password]
  (not (nil? (re-matches #"[a-z]{8}" password)))
  )

(defn- ^Character following_char [^Character in_char]
  (char (inc (int in_char)))
  )

(defn- ^Integer max_increasing_straight_length [^CharSequence password ^Character previous_char ^Integer max_straight_length ^Integer current_straight_length]
  (let [^Character next_char (first password)
        current_length_inc (inc current_straight_length)
        ]
    (cond
      (empty? password)
      max_straight_length

      (= (following_char previous_char) next_char)
      (max_increasing_straight_length (rest password) next_char
                                      (max current_length_inc max_straight_length) current_length_inc)

      :else
      (max_increasing_straight_length (rest password)
                                      next_char max_straight_length 1)
      )
    )
  )

; Passwords must include one increasing straight of at least three letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
(defn- ^Boolean has_increasing_straight [^CharSequence password ^Integer min_length]
  (>= (max_increasing_straight_length (rest password) (first password) 1 1) min_length)
  )

; Passwords may not contain the letters i, o, or l, as these letters can be mistaken for other characters and are therefore confusing.
(defn- ^Boolean without_confusing_characters [^CharSequence password]
  (nil? (re-find #"[iol]" password))
  )

; Passwords must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz.
(defn- ^Boolean has_enough_non_overlapping_pairs [^CharSequence password ^Integer min_pairs]
  (>= (count (re-seq #"(.)\1{1}" password)) min_pairs)
  )

(defn ^Boolean valid_password? [^CharSequence password]
  (and
    (validate-base-password-format password)
    (has_increasing_straight password 3)
    (without_confusing_characters password)
    (has_enough_non_overlapping_pairs password 2)
    )
  )

(defn- ^CharSequence increase_password [^CharSequence password]
  (if (empty? password)
    password
    (let [oldLast (last password)
          remainingPassword (subs password 0 (dec (count password)))]
      (if (= oldLast \z)
        (str (increase_password remainingPassword) \a)
        (str remainingPassword (following_char oldLast))
        )
      )
    )
  )

(defn ^CharSequence santas_next_password [^CharSequence oldPassword]
  (loop [password (increase_password oldPassword)]
    (if (valid_password? password)
      password
      (recur (increase_password password)))
    )
  )

