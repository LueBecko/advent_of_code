(ns advent-of-code.aoc2015
  (:require [clojure.java.io :as io])
  )

(defn parse-int [number-string]
  (Integer/parseInt (re-find #"\A-?\d+" number-string)))

(defn read-lines [filename]
  (with-open [reader (io/reader filename)]
    (let [line (line-seq reader)]
      (mapv identity line)
      )
    )
  )
